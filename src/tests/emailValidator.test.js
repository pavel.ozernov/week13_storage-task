const assert = require('chai').assert;
const sinon = require('sinon');
import { validate } from '../email-validator.js';
import { validateAsync} from '../email-validator.js';
import { validateWithThrow} from '../email-validator.js';

import {validateWithLog} from '../email-validator.js';

import { expect } from 'chai'; 

//===================first test trial
 
describe('first test', () => { 
  it('should return 2', () => {
    expect(2).to.equal(2);
  }) 
});

//===================email-validator BASE

describe('email-validator - basic functionality', () => {
  it('should return false for an empty string input', () =>{
  expect(validate('')).to.be.false;   
  });

  it('should return false for an well structrured but invalid email input (other than gmail.com and outlook.com)', () =>{
    expect(validate('123123@mail.ru')).to.be.false;   
  });

  it('should return false for email input with spaces within', () =>{
      expect(validate('12 3123@mail.ru')).to.be.false;   
  });

  it('should return false for an invalid email ending', () =>{
    expect(validate('123123@gmail.ru')).to.be.false;   
  });
  
  it('should return true for well structured gmail.com ending', () =>{
      expect(validate('123123@gmail.com')).to.be.true;   
  });

  it('should return true for well structured outlook.com ending', () =>{
    expect(validate('123123@outlook.com')).to.be.true;   
  });
  

});

//==================validateAsync

describe("validateAsync", function() {
  it("should return true for a valid email", async function() {
    const isValid = await validateAsync("example@gmail.com");
    assert.strictEqual(isValid, true);
  });

  it("should return false for an invalid email", async function() {
    const isValid = await validateAsync("example@gmail.ru");
    assert.strictEqual(isValid, false);
  });

  it('should return false for an empty string input', async function() {
    const isValid = await validateAsync("");
    assert.strictEqual(isValid, false);
  });   
    
});

//===========validateWithThrow

describe("validateWithThrow", function() {
  it("should return true for a valid email", () => {
    const isValid = validateWithThrow("example@gmail.com");
    assert.strictEqual(isValid, true);
  });

  it('should throw an error when an invalid email is provided', () => {
    assert.throws(() => {
      validateWithThrow('example@mail.ru');
    }, /The provided email is invalid/);
  });

});

//============console.log

const VALID_EMAILS = ['john.doe@gmail.com', 'jane.doe@outlook.com'];
const INVALID_EMAILS = ['jane.doe@example', 'john.doe', 'john.doe@'];
const VALID_EMAIL_ENDINGS = ['example.com'];

describe('validateWithLog', function() {
  let consoleLogStub;

  beforeEach(function() {
    // Replace console.log with a stub that captures its output
    consoleLogStub = sinon.stub(console, 'log');
  });

  afterEach(function() {
    // Restore console.log after each test
    consoleLogStub.restore();
  });

  it('should return true for valid emails', function() {
    VALID_EMAILS.forEach(function(email) {
      const result = validateWithLog(email);
      assert.strictEqual(result, true);
      assert(consoleLogStub.calledWith(`Email validation result for "${email}": true`));
    });
  });

  it('should return false for invalid emails', function() {
    INVALID_EMAILS.forEach(function(email) {
      const result = validateWithLog(email);
      assert.strictEqual(result, false);
      assert(consoleLogStub.calledWith(`Email validation result for "${email}": false`));
    });
  });
});